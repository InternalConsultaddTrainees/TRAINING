import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterializeModule } from 'angular2-materialize';


import { AppComponent } from './app.component';
import { TestcompComponent } from './testcomp/testcomp.component';
import { NewcompComponent } from './newcomp/newcomp.component';
import { ClickComponent } from './click/click.component';
import { BlurComponent } from './blur/blur.component';
import { KeydownComponent } from './keydown/keydown.component';

@NgModule({
  declarations: [
    AppComponent,
    TestcompComponent,
    NewcompComponent,
    ClickComponent,
    BlurComponent,
    KeydownComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
