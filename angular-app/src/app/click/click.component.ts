import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-click',
  templateUrl: './click.component.html',
  styleUrls: ['./click.component.css']
})

export class ClickComponent implements OnInit {
 
   clickMessage =' ';
  constructor() { }

  ngOnInit() { }

  Onclick() {
    if(this.clickMessage==' ')
    this.clickMessage = "On Click Event Called !!";
    else this.clickMessage = " ";
  }

}
