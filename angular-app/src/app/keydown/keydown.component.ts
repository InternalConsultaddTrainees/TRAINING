import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-keydown',
  templateUrl: './keydown.component.html',
  styleUrls: ['./keydown.component.css']
})
export class KeydownComponent implements OnInit {
   count :any;
  constructor() { 
    this.count = 0;
  }

  ngOnInit() {
  }
  keydown()
  {
     this. count = this.count + 1;
  }

}
