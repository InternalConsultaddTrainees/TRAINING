import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-c4',
  templateUrl: './c4.component.html',
  styleUrls: ['./c4.component.css']
})
export class C4Component implements OnInit {

  constructor(private router: Router) { }

  c1() {
    this.router.navigate(['/C1']);
  }

  c2() {
    this.router.navigate(['/C2']);
  }

  c3() {
    this.router.navigate(['/C3']);
  }
  home() {
    this.router.navigate(['/']);
  }
  ngOnInit() {
  }

}
