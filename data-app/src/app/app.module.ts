import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { C1Component } from './c1/c1.component';
import { C2Component } from './c2/c2.component';
import { C3Component } from './c3/c3.component';
import { C4Component } from './c4/c4.component';
import { SlideComponent } from './slide/slide.component';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'C1', component: C1Component },
  { path: 'C2', component: C2Component },
  { path: 'C3', component: C3Component },
  { path: '', component: SlideComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    C1Component,
    C2Component,
    C3Component,
    C4Component,
    SlideComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    Ng2CarouselamosModule,
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
