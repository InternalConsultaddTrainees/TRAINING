import { Component, OnInit } from '@angular/core';
import { CarouselModule } from 'angular4-carousel';


@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css']
})
export class SlideComponent implements OnInit {
  items: Array<any> = []

  constructor() {
    this.items = [
      { name: 'assets/img/1.jpg' },
      { name: 'assets/img/2.jpg' },
      { name: 'assets/img/3.jpg' },
      { name: 'assets/img/4.jpg' },
      { name: 'assets/img/1.jpg' },
      { name: 'assets/img/2.jpg' },
      { name: 'assets/img/3.jpg' },
      { name: 'assets/img/4.jpg' },
     
    ]
   }

  ngOnInit() {
  }

}
