var http = require('http');
var fs = require('fs');
var file = process.argv;
http.createServer(function (req, res) {
  
  //Open a file on the server and return it's content:
  fs.readFile(file[2], function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    console.log(data);
    //exports.data= data;
    res.write(data);
    return res.end();
  });
}).listen(7080);
